import re

ZERO=chr(int("2062", 16))
ONE=chr(int("2063", 16))


# Convert string to binary
def strbin(s):
    return ''.join(format(ord(i),'0>8b') for i in s)

# Convert 1s and 0s in a string to invisible unicode characters
def encode(s):
#     Convert string to binary
    sBin=strbin(s)
#     Replace zeros and ones with their invisible representations
    sBin=sBin.replace("0", ZERO)
    sBin=sBin.replace("1", ONE)
#     Return invisible string
    return(sBin)

# Convert invisible representation of text back to text
def decode(sBin):
#     Replace invisible characters with zeros and ones
    sBin=sBin.replace(ZERO, "0")
    sBin=sBin.replace(ONE, "1")
    # Convert binary to string
    s=''.join(chr(int(sBin[i*8:i*8+8],2)) for i in range(len(sBin)//8))
    return(s)

def watermark(text, mark, freq=100):
    # Encode mark
    hiddenMark=encode("WaterText: ") + encode(mark) + encode(" :WaterText")
    # Only mark strings shorter than marking frequency once, otherwise mark throughout
    if len(text)<freq:
        markedText = hiddenMark + text
    else:
        # Generate regex to match correct number of characters
        regex='.?'*freq
        # Add mark every freq characters
        markedText=hiddenMark.join(re.findall(regex, text))
    return markedText

# Extract and decode watermark from within text
def extract(text):
#     Find all instances of the hidden characters in text and concatenate them into string
    mark="".join(re.findall(f"[{ZERO},{ONE}]", text))
#     Decode all data present
    mark=decode(mark)
#     Look for text enclosed in headers and footers
    fullMarks = re.findall("WaterText: (.*?) :WaterText", mark)
#     Return first watermark if all are the same
    if len(fullMarks) > 0 and all(x == fullMarks[0] for x in fullMarks):
        mark=fullMarks[0]
#     Return all watermarks if they differ
    elif not all(x == fullMarks[0] for x in fullMarks):
        mark=fullMarks
#     Warn user that the mark was damaged and return raw text
    else:
        print("Damaged or missing header/footer. Returning raw mark")

#     Decode and return watermark
    return mark

# Watermark a file
def watermarkFile(path, mark):
    with open(path, "r+") as f:
#         Get contents of file
        fileText=f.read()
#         Watermark text
        fileText=watermark(fileText, mark)
#         Write watermarked text back to file
        f.seek(0)
        f.write(fileText)
#         Close file
        f.close()

# Extract watermark from file
def extractFile(path):
    with open(path, "r") as f:
#         Get contents of file
        fileText=f.read()
#         Return extracted text
        return(extract(fileText))

