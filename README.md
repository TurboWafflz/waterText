# WaterText

WaterText is a Python module for hiding invisible watermarks in text. It does this by representing the text using unicode characters 0x2062 and 0x2063, both of which are completely invisible in many text editors and web browsers. While this is easy to detect and remove if you know what you're looking for or use a text editor that renders these, it's still hard to notice in a lot of conditions

Usage:
```py
import waterText

# Encode some text. If printed directly to your terminal, will likely display as escape sequences
waterText.encode("some text")

# Decode some text
waterText.decode("Some encoded text, I could put an example of actual encoded text here but it would be invisible so what's the point")

# Watermark some text, inserting invisibly encoded text every 100 characters throughout it
waterText.watermark("This sure is some cool text", "My evil secrets")

# Decode encoded text mixed with actual text, returns a string if only one mark is present or a list if multiple different marks are present
waterText.extract("This is a perfectly ⁢⁣⁢⁣⁢⁣⁣⁣⁢⁣⁣⁢⁢⁢⁢⁣⁢⁣⁣⁢⁣⁢⁢⁣⁢⁣⁣⁣⁢⁣⁢⁢⁢⁢⁣⁢⁢⁢⁢⁢⁢⁣⁣⁢⁣⁣⁣⁢⁢⁣⁣⁢⁣⁣⁣⁣⁢⁢⁣⁢⁢⁢⁢⁢⁢⁣⁣⁢⁣⁢⁢⁣⁢⁣⁣⁣⁢⁣⁢⁢⁢⁢⁣⁢⁢⁢⁢⁢⁢⁣⁣⁢⁣⁢⁢⁣⁢⁣⁣⁣⁢⁢⁣⁣⁢⁣⁣⁢⁣⁣⁣⁢⁢⁢⁣⁢⁢⁣⁣⁣⁢⁣⁣⁣⁢⁣⁢⁢normal text file⁢⁣⁢⁣⁢⁣⁣⁣⁢⁣⁣⁢⁢⁢⁢⁣⁢⁣⁣⁢⁣⁢⁢⁣⁢⁣⁣⁣⁢⁣⁢⁢⁢⁢⁣⁢⁢⁢⁢⁢⁢⁣⁣⁢⁣⁣⁣⁢⁢⁣⁣⁢⁣⁣⁣⁣⁢⁢⁣⁢⁢⁢⁢⁢⁢⁣⁣⁢⁣⁢⁢⁣⁢⁣⁣⁣⁢⁣⁢⁢⁢⁢⁣⁢⁢⁢⁢⁢⁢⁣⁣⁢⁣⁢⁢⁣⁢⁣⁣⁣⁢⁢⁣⁣⁢⁣⁣⁢⁣⁣⁣⁢⁢⁢⁣⁢⁢⁣⁣⁣⁢⁣⁣⁣⁢⁣⁢⁢")

# Add a watermark to a text file
waterText.watermarkFile("superCool.txt", "This file is property of SuperCool Inc. Don't steal it or you're not super cool")

## Extract watermark from a file,
waterText.extractFile("superCool.txt")

```
